var DisableBackup = function() {};

DisableBackup.prototype.disable = function(successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, 'DisableBackup', 'disable', []);
};

module.exports = new DisableBackup();