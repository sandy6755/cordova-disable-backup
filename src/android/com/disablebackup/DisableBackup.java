package com.disablebackup; // Replace with your app's package name

import android.app.Application;
import android.content.Context;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONException;

public class DisableBackup extends CordovaPlugin {

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        // Add any initialization code if needed
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if ("disable".equals(action)) {
            disableBackup(callbackContext);
            return true;
        }
        return false;
    }

    private void disableBackup(CallbackContext callbackContext) {
        try {
            Context appContext = cordova.getActivity().getApplicationContext();
            Application app = cordova.getActivity().getApplication();
            appContext.getPackageManager().setComponentEnabledSetting(app.getComponentName(), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            callbackContext.success();
        } catch (Exception e) {
            callbackContext.error("Error disabling backup: " + e.getMessage());
        }
    }
}
